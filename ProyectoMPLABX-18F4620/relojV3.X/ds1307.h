//Esta biblioteca de funciones est� hecha en base a una biblioteca similar
//que fue proporcionada por la empresa SEMAVENCA, aparece tambi�n en la p�gina
//web: http://microembebidos.com/2013/09/17/tutorial-c18-reloj-ds1307/
//
//Realizada por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingenier�a, 
//Escuela de Ingenier�a de Sistemas
//Para el proyecto: Actualizaci�n de reloj de tiempo real mediante GPS
//De la empresa: Sem�foros de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015
#include <i2c.h>

#define DIR 0XD0
#define DIR0 0X00

//transmiteDato env�a por el bus de datos del I2C el byte recibido por 
//par�metro mediante la funci�n WriteI2C de la biblioteca i2c.h
unsigned char transmiteDato(unsigned char dato)
{
    unsigned char estado, buff;
    
    buff = SSPBUF;
    estado = WriteI2C(dato);    
    if(estado == -1){
        buff = SSPBUF;
        SSPCON1bits.WCOL = 0;
        return(-1);
    }
    else if(estado == -2)
        return(-2);
    return(0);
}

//leeDato realiza la operaci�n de lectura de la direcci�n recibida por
//par�metro del RTC generando las condiciones necesarias y usando la funci�n
//ReadI2C de la biblioteca i2c.h
unsigned int leeDato(unsigned char address)
{
    unsigned int dato;
    IdleI2C();
    StartI2C();
    if(PIR2bits.BCLIF)
        return(-1);
    if(transmiteDato(DIR) != 0)
        return(-2);
    if(transmiteDato(address) != 0)
        return(-2);
    RestartI2C();
    IdleI2C();
    if(transmiteDato(DIR | 0X01) != 0)
        return(-2);
    dato = (unsigned int) ReadI2C();
    NotAckI2C();
    while(SSPCON2bits.ACKEN);
    StopI2C();
    return(dato);
}

//escribeDato genera las condiciones necesarias para realizar la operaci�n
//de escritura del par�metro data en la direcci�n address del RTC
unsigned char escribeDato(unsigned char address, unsigned char data)
{
    IdleI2C();
    StartI2C();
    if(PIR2bits.BCLIF)
        return(-1);
    if(transmiteDato(DIR) != 0)
        return(-2);
    if(transmiteDato(address) != 0)
        return(-2);
    if(transmiteDato(data) != 0)
        return(-2);
    StopI2C();
    return(0);
}

//inicializaRTC env�a al RTC los datos de operaci�n iniciales para que este
//comience el conteo
unsigned char inicializaRTC(unsigned char segundos, unsigned char minutos,
                            unsigned char horas, unsigned char dia,
                            unsigned char fecha, unsigned char mes,
                            unsigned char ano, unsigned char ctrl)
{
    IdleI2C();
    StartI2C();
    if ( PIR2bits.BCLIF )
        return ( -1 );
    if(transmiteDato(DIR) != 0)
        return(-2);
    if(transmiteDato(DIR0) != 0)
        return(-2);
    if(transmiteDato(segundos) != 0)
        return(-2);
    if(transmiteDato(minutos) != 0)
        return(-2);
    if(transmiteDato(horas) != 0)
        return(-2);
    if(transmiteDato(dia) != 0)
        return(-2);
    if(transmiteDato(fecha) != 0)
        return(-2);
    if(transmiteDato(mes) != 0)
        return(-2);
    if(transmiteDato(ano) != 0)
        return(-2);
    if(transmiteDato(ctrl) != 0)
        return(-2);
    StopI2C();
    return (0);
}
