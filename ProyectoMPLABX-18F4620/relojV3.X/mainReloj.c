//Este programa fue hecho con c�digos proporcionados por la empresa SEMAVENCA
//
//Desarrollado por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingenier�a, 
//Escuela de Ingenier�a de Sistemas
//Para el proyecto: Actualizaci�n de reloj de tiempo real mediante GPS
//De la empresa: Sem�foros de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015

#include <xc.h>
#include "usart.h"
#include "delays.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <i2c.h>
#include "ds1307.h"

//CONFIG1H
#pragma config OSC = INTIO67
#pragma config FCMEN = ON
#pragma config IESO = ON
//CONFIG2L
#pragma config PWRT = OFF
#pragma config BOREN = SBORDIS
#pragma config BORV = 3
//CONFIG2H
#pragma config WDT = OFF
#pragma config WDTPS = 32768
//CONFIG3H
#pragma config CCP2MX = PORTC
#pragma config PBADEN = ON
#pragma config LPT1OSC = ON
#pragma config MCLRE = ON
//CONFIG4L
#pragma config STVREN = ON
#pragma config LVP = OFF
#pragma config XINST = OFF
//CONFIG5L
#pragma config CP0 = OFF
#pragma config CP1 = OFF 
#pragma config CP2 = OFF
#pragma config CP3 = OFF
//CONFIG5H
#pragma config CPB = OFF
#pragma config CPD = OFF
//CONFIG6L
#pragma config WRT0 = OFF
#pragma config WRT1 = OFF
#pragma config WRT2 = OFF
#pragma config WRT3 = OFF
//CONFIG6H
#pragma config WRTC = OFF
#pragma config WRTB = OFF
#pragma config WRTD = OFF
//CONFIG7L
#pragma config EBTR0 = OFF
#pragma config EBTR1 = OFF
#pragma config EBTR2 = OFF
#pragma config EBTR3 = OFF
//CONFIG7H
#pragma config EBTRB = OFF

#define LED 0X01   //Pin usado para encendido-apagado del led
#define VAL_SPBRG 51   //Valor para establecer la velocidad de comunicaci�n EUSART
//Macros de inicializaci�n del RTC
#define HORA 12
#define MINUTOS 0
#define SEGUNDOS 0
#define ANIO 15
#define MES 7
#define DIA 31
#define CTRL 0X10 
#define FECHA 7
#define CONT_I2C 0X08   //Valor usado para controlar la velocidad del I2C
//Macros de pines y funciones usados con el 74HC595
#define RELOJ 0X01
#define DATO 0X02
#define LATCH 0X04
#define MR 0X08
#define OE 0X10
#define ACTRELOJ() LATD=LATD|RELOJ
#define ACTDATO() LATD=LATD|DATO
#define ACTLATCH() LATD=LATD|LATCH
#define DACTRELOJ() LATD=LATD&(~RELOJ) 
#define DACTDATO() LATD=LATD&(~DATO)
#define DACTLATCH() LATD=LATD&(~LATCH)

//Arreglo de valores para representar n�meros en la tarjeta de d�gitos
//la posici�n del valor equivale al n�mero que representa
unsigned char num[]={0XFC, 0X60, 0XDA, 0XF2, 0X66, 0XB6, 0XBE, 0XE0, 0XFE, 0XE6};

//Definici�n de funciones
int bcd(int dec);
int decimal(int bcd);
void inicializar();
void procesaTrama(int *h, int *m, int *s);
void corrigeHora(int *h, int *m, int *s);
void actualizaRTC(int hAct, int mAct, int sAct);
void descartaDato();
void almacenaReg(unsigned char valor);
void activaSegmentos(int val1, int val2);

//Rutina de interrupci�n, contiene las instrucciones para procesar caracteres
//recibidos en el perif�rico EUSART
void interrupt sentenciasISR(void)
{    
    int h0, m0, s0;
    char c;
    char tramaGPS[8];
    
    if(PIR1bits.RCIF){
        PIR1bits.RCIF=0;
        c=ReadUSART();
        
        if(c=='$'){
            getsUSART(tramaGPS,6);
            if(strstr(tramaGPS,"GGA") || strstr(tramaGPS,"RMC")){
                procesaTrama(&h0,&m0,&s0);
                actualizaRTC(h0,m0,s0);
            }
        }
        else
            descartaDato();        
    }
}

//Funci�n principal
void main(void)
{
    int hant = 0;
    int mant = 0;
    int h, m, s;    
    int cont = 0;
    int encendido=0;

    inicializar();

    while(1){
        //Al recibir alguna trama la procesa y actualiza el RTC
        
        //Encendido del led cada cierto tiempo
        if(!encendido && cont<2000){
            LATE=LATE|LED;
            encendido=1;
        }
        if(encendido && 2000<cont){
            LATE=LATE&(~LED);
            encendido=0;                
        }
        if(3000<cont)
            cont=0;
        if(cont%1000==0){
            //Lectura de datos del RTC
            h = decimal(leeDato(0X02)&~(0X60));   //Almacena hora
            m = decimal(leeDato(0X01));   //Almacena minutos
            s = decimal(leeDato(0X00));   //Almacena segundos            
            
        }
        //Habilitaci�n-Deshabilitaci�n de la interrupci�n durante el per�odo
        //de tiempo establecido
        if(s%30==0){
            PIE1bits.RCIE=1;
            RCSTAbits.CREN=1;
            Delay10KTCYx(1); 
            PIE1bits.RCIE=0;
            RCSTAbits.CREN=0;
        }

        //Encende los 7 segmentos
        if(h!=hant || m!=mant)
            activaSegmentos(h, m);
        
        mant=m;
        hant=h;
        cont++;
    }
    CloseUSART();
    CloseI2C();
}

//bcd es usada para convertir datos del formato decimal al formato BCD
int bcd(int dec)
{
    return ((dec/10)<<4)+(dec%10);
}

//decimal es usada para convertir datos del formato BCD al formato decimal
int decimal(int bcd)
{
    return ((bcd>>4)*10)+bcd%16;
}

//inicializar cotiene las instrucciones de configuraci�n e inicio para los
//puertos, perif�ricos y dispositivos
void inicializar()
{
    //Selecci�n de la frecuencia del oscilador interno
    OSCCON = 0XF3;
    //Configiraci�n de los puertos
    TRISA = 0b11111111;
    TRISB = 0b11111111;
    TRISC = 0b10111111;
    TRISD = 0b11100000;
    TRISE = 0b1110;
    //Inicializaci�n de los pines
    LATA = 0X00;
    LATB = 0X00;
    LATC = 0x00;
    LATD = 0X00;
    LATE = 0X00;
    //Comunicaci�n EUSART, modo as�ncrono, 8 bits, interrupci�n debido a 
    //recepci�n, recepci�n continua, velocidad de transmisi�n a 9600 baudios
    OpenUSART(  USART_TX_INT_OFF & USART_RX_INT_ON & USART_ASYNCH_MODE &
                USART_EIGHT_BIT & USART_CONT_RX & USART_BRGH_HIGH, VAL_SPBRG);

    //Comunicaci�n I2C, maestro, velocidad a 100KHz
    OpenI2C(MASTER,SLEW_OFF);
    SSPADD = CONT_I2C;
    
    //Inicializaci�n del RTC    
    //inicializaRTC(bcd(SEGUNDOS), bcd(MINUTOS), bcd(HORA), bcd(FECHA), 
    //              bcd(DIA), bcd(MES), bcd(ANIO),CTRL);
    //IPEN: Bit de habilitaci�n de niveles de prioridad de interrupci�n
    RCONbits.IPEN=1;
    //RCIP: Bit de nivel de prioridad de interrupci�n por recepci�n en el
    //periferico EUSART, requiere que el bit IPEN est� activo,
    //se selecciona 1 para alta prioridad
    IPR1bits.RCIP=1;
    //GIEH: Bit de habilitaci�n de interrupciones globales, debido a que IPEN
    //es 1,habilita todas las interrupciones de alta prioridad
    INTCONbits.GIEH=1;
    //RCIE: Habilita la interrupci�n por recepci�n en el periferico EUSART
    PIE1bits.RCIE=1; 
    //RCIF: Habilita el bit bandera de la interrupci�n por recepci�n en el
    //periferico EUSART, se activa cuando ocurre la interrupci�n, debe  
    //limpiarse antes y despues de la interrupci�n
    PIR1bits.RCIF=0;
    //Activaci�n de los registros de desplazamiento
    LATD = LATD | MR;
    LATD = LATD & ~OE;
    //Activaci�n de la tarjeta de d�gitos
    activaSegmentos(0, 0);
}

//procesaTrama es llamada al comenzar la interrupci�n por el periferico EUSART
//extrae los caracteres entrantes en el puerto y separa los datos necesarios, 
//en este caso separa hora, minutos y segundos y los almacena como valores 
//decimales en los parametros pasados como referencia
void procesaTrama(int *h, int *m, int *s)
{  
    char hora[3];
    char minutos[3];
    char segundos[3];
    
    //Ejemplo de trama GGA:
    //$GPGGA,105540,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
    //Se extrae de la trama recibida la hora, minutos y segundos
    getsUSART(hora,2);
    getsUSART(minutos,2);
    getsUSART(segundos,2);
    segundos[2] = '\0';
    minutos[2] = '\0';
    hora[2] = '\0';
    
    while(!DataRdyUSART());
    descartaDato();
    
    //Convierte string a entero
    *h=atoi(hora);    
    *m=atoi(minutos);
    *s=atoi(segundos);
}

//corrigeHora recibe los datos de la hora del gps en formato UTC y los
//ajusta a la hora equivalente en Venezuela, esta funci�n es llamada
//en la funci�n actualizaRTC
void corrigeHora(int *h, int *m, int *s)
{
    if(*h<05)
        *h = (12 + *h) - 4;
    else if(04<*h && *h<17)
        *h = *h - 4;
    else if(16<*h)
        *h = (*h - 12) - 4;
    if(29<*m)
        *m = *m - 30;
    else if(*m<30){
        *m = *m + 30;
        *h = *h - 1;
        if(*h==0)
            *h = *h + 12;
    }
}

//actualizaRTC recibe la hora del gps, verifica si es horario am o pm, luego
//llama a la funci�n corrigeHora y luego env�a los datos por el puerto I2C para
//actualizar el RTC, usando la funci�n Escribe_ds1307 de la librer�a ds1307.h
void actualizaRTC(int hAct, int mAct, int sAct)
{
    unsigned char ampm;
    
    ampm = 0X40;
    if(11<(hAct-4) || (hAct-4)<0)
        ampm = ampm | 0X20;

    corrigeHora(&hAct, &mAct, &sAct);

    escribeDato(0X02, bcd(hAct)|ampm);
    escribeDato(0X01, bcd(mAct));
    escribeDato(0X00, bcd(sAct));
}

//descartaDato lee los datos del perif�rico EUSART con la finalidad de liberar
//el puerto de recepci�n
void descartaDato()
{    
    char dato[2];
    if(DataRdyUSART())
        while(1){
            dato[0] = ReadUSART();            
            if(dato[0]=='*'){
                getsUSART(dato,2);                
                return;
            }            
            while(!DataRdyUSART());
        }    
}

//almacenaReg env�a al registro de desplazamiento las secuencias de bits
//para representar los valores le�dos del RTC en la tarjeta de d�gitos
void almacenaReg(unsigned char valor)
{    
    for(int i=0; i<8; i++){       
        ACTDATO();
        if((valor&0x01)==0X00)
            DACTDATO();
        valor = valor >> 1;
        Delay1TCYx(5);
        ACTRELOJ();
        Delay1TCYx(5);
        DACTRELOJ();
    }
}

//activaSegmentos hace el llamado de la funci�n almacenaReg, una vez que se
//han almacenado las secuencias de bits en los registros de desplazamiento
//correspondientes, se activa el bit LATCH, haciendo que estos bits pasen y se
//mantengan en los pines de salida paralela hasta recibir algun cambio
void activaSegmentos(int val1, int val2)
{
    char val[2];
    int u,d;

    sprintf(val,"%.2i",val1);
    
    u = val[1] -'0';
    d = val[0] -'0';
    
    almacenaReg(num[d]);
    almacenaReg(num[u]);

    sprintf(val,"%.2i",val2); 

    u = val[1] -'0';
    d = val[0] -'0';

    almacenaReg(num[d]);
    almacenaReg(num[u]);
    
    ACTLATCH();
    Delay1TCYx(5);
    DACTLATCH();
}