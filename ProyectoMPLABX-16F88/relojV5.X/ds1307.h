//Esta biblioteca de funciones est� hecha en base a una biblioteca similar
//que fue proporcionada por la empresa SEMAVENCA, aparece tambi�n en la p�gina
//web: http://microembebidos.com/2013/09/17/tutorial-c18-reloj-ds1307/
//
//Realizada por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingenier�a, 
//Escuela de Ingenier�a de Sistemas
//Para el proyecto: Actualizaci�n de reloj de tiempo real mediante GPS
//De la empresa: Sem�foros de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015#include "i2c_P16F88.h"

#define DIR 0XD0
#define DIR0 0X00

//leeDato realiza la operaci�n de lectura de la direcci�n recibida por
//par�metro del RTC generando las condiciones necesarias y usando la funci�n
//ReadI2C de la biblioteca i2c_P16F88.h
unsigned int leeDato(unsigned char address)
{
    unsigned int dato;

    StartI2C();  
    WriteI2C(DIR);    
    WriteI2C(address);
    StartI2C();
    WriteI2C(DIR | 0X01);
    dato = (unsigned int) ReadI2C();
    StopI2C();

    return(dato);
}

//escribeDato genera las condiciones necesarias para realizar la operaci�n
//de escritura del par�metro data en la direcci�n address del RTC
unsigned char escribeDato(unsigned char address, unsigned char data)
{
    StartI2C();
    WriteI2C(DIR);
    WriteI2C(address);
    WriteI2C(data);
    StopI2C();

    return(0);
}

//inicializaRTC env�a al RTC los datos de operaci�n iniciales para que este
//comience el conteo
unsigned char inicializaRTC(unsigned char segundos, unsigned char minutos,
                            unsigned char horas, unsigned char dia,
                            unsigned char fecha, unsigned char mes,
                            unsigned char ano, unsigned char ctrl)
{    
    StartI2C();
    WriteI2C(DIR);
    WriteI2C(DIR0);
    WriteI2C(segundos);
    WriteI2C(minutos);
    WriteI2C(horas);
    WriteI2C(dia);
    WriteI2C(fecha);
    WriteI2C(mes);
    WriteI2C(ano);
    WriteI2C(ctrl);
    StopI2C();

    return(0);
}
