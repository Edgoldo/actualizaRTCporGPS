//Esta biblioteca de funciones est� hecha en base a una biblioteca similar
//desarrollada por Microchip Technololy Inc. para el compilador XC8
//de nombre usart.h, fue adaptada para usarse en este proyecto
//
//Realizada por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingenier�a, 
//Escuela de Ingenier�a de Sistemas
//Para el proyecto: Actualizaci�n de reloj de tiempo real mediante GPS
//De la empresa: Sem�foros de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015

#ifndef USART_P16F88_H
#define	USART_P16F88_H

#ifndef __USART_H
#define __USART_H

#include <htc.h>

#define VAL_SPBRG 51        //Valor necesario para ganerar una tasa de bauds
                            //de 9.6K baudios

/* ***** usart (TXSTA, RCSTA) ***** */
void OpenUSART (void);
char DataRdyUSART (void);
char ReadUSART (void);
void WriteUSART (char data);
void getsUSART (char *buffer,  unsigned char len);
void putsUSART (char *data);
char BusyUSART (void);
void CloseUSART (void);

#endif

#endif	/* USART_P16F88_H */

