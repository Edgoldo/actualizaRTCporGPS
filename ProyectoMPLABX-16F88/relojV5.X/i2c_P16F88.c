//Esta biblioteca de funciones est� hecha en base a un conjunto de bibliotecas
//que contienen funciones similares, fueron desarrolladas por Microchip 
//Technololy Inc. para el compilador XC8 y adaptadas para usarse en 
//este proyecto
//
//Realizada por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingenier�a, 
//Escuela de Ingenier�a de Sistemas
//Para el proyecto: Actualizaci�n de reloj de tiempo real mediante GPS
//De la empresa: Sem�foros de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015

#include <pic16f88.h>
#include "i2c_P16F88.h"

//StartI2C genera la condici�n de inicio de comunicaci�n serial por medio del
//env�o de se�ales a los pines usados para establecer la comunicaci�n I2C
void StartI2C( void )
{
    PORTB = I2C_SDA | I2C_SCL;
    _delay(5);
    PORTB = I2C_SCL & ~(I2C_SDA);
    _delay(6);
}

//StopI2C genera la condici�n de parada de la comunicaci�n I2C usando se�ales
//sobre los buses establecidos
void StopI2C( void )
{
    PORTB = 0X00;
    PORTB = I2C_SCL;
    _delay(5);
    PORTB = I2C_SDA | I2C_SCL;
    _delay(6);
}

//WriteI2C genera los pulsos correspondientes sobre los buses de reloj y dato
//para enviar el byte de datos recibido por par�metro, genera al final un pulso
//extra sobre el bus de reloj para recibir el bit de reconocimiento
void WriteI2C( unsigned char data_out )
{
    unsigned char aux;
    
    PORTB = 0X00;
    for(int i=0; i<8; i++){       
        aux = 0X00;
        aux |= I2C_SDA;
        if((data_out&0X80)==0X00)
            aux &= ~I2C_SDA;
        data_out=data_out << 1;
        PORTB = aux;
        PORTB = aux | I2C_SCL;
        _delay(5);
        PORTB = aux & ~(I2C_SCL);
        _delay(6);
    }
    PORTB = I2C_SCL;
    PORTB = ~(I2C_SCL);
}

//ReadI2C genera las se�ales sobre los buses de reloj y dato necesarias para 
//recibir un byte de datos por medio del bus I2C, una vez recibido genera
//el bit de no reconocimiento y devuelve la palabra
unsigned char ReadI2C( void )
{
    unsigned char aux;
    int desp = 0;
    
    aux = I2C_SDA;
    while((aux & 0X01) == 0){
        desp++;
        aux = aux >> 1;
    }
    
    aux = 0X00;
    TRISB = estadoB | I2C_SDA;
    PORTB = 0X00;
    
    for(int i=0; i<8; i++){        
        aux = aux << 1;
        PORTB = I2C_SCL;
        aux = aux | ((PORTB & I2C_SDA) >> desp);
        _delay(5);
        PORTB = ~(I2C_SCL);
        _delay(5);
    }
    TRISB = estadoB & ~(I2C_SDA | I2C_SCL);
    PORTB = I2C_SDA;
    _delay(5);
    PORTB = I2C_SDA | I2C_SCL;
    _delay(5);
    PORTB = I2C_SDA & ~(I2C_SCL);
    TRISB = estadoB;
    return (aux);
}
