//Este programa fue hecho con c�digos proporcionados por la empresa SEMAVENCA
//
//Desarrollado por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingenier�a, 
//Escuela de Ingenier�a de Sistemas
//Para el proyecto: Actualizaci�n de reloj de tiempo real mediante GPS
//De la empresa: Sem�foros de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015

#include <xc.h>
#include <pic16f88.h>
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "usart_P16F88.h"
#include "i2c_P16F88.h"
#include "ds1307.h"

//CONFIG1
#pragma config WDTE = OFF
#pragma config PWRTE = OFF
#pragma config FOSC = INTOSCIO
#pragma config MCLRE = ON
#pragma config BOREN = OFF
#pragma config LVP = OFF
#pragma config CPD = OFF
#pragma config WRT = OFF
#pragma config CCPMX = RB3
#pragma config CP = OFF
//CONFIG2
#pragma config FCMEN = ON
#pragma config IESO = ON

#define LED 0X80//PORTBbits.RB7 //Pin usado para encendido-apagado del led
//Macros de direcci�n para el RTC
#define DIRSEG 0X00
#define DIRMIN 0X01
#define DIRHOR 0X02
//Macros de inicializaci�n de tiempo para el RTC
#define HORA 12
#define MINUTOS 0
#define SEGUNDOS 0
#define ANIO 15
#define MES 7
#define DIA 31
#define CTRL 0X10
#define FECHA 7
//Tramas a reconocer
#define TRAMA1 "GGA"
#define TRAMA2 "RMC"
//Macros del puerto usado con el 74HC595
#define RELOJ 0X01
#define DATO 0X02
#define LATCH 0X04
#define MR 0X08
#define OE 0X10
#define ACTLATCH() PORTA = (LATCH | MR) & ~OE
#define DACTLATCH() PORTA = MR & ~(LATCH | OE)
#define ACTMR() PORTA = MR & ~OE

//Arreglo de valores para representar n�meros en la tarjeta de d�gitos
//la posici�n del valor equivale al n�mero que representa
unsigned char num[] = {0XFC, 0X60, 0XDA, 0XF2, 0X66, 0XB6, 0XBE, 0XE0, 0XFE, 0XE6};
int h0 = 0;   //Almacena la hora recibida de las tramas
int m0 = 0;   //Almacena los minutos recibidos de las tramas
int s0 = 0;   //Almacena los segundos recibidos de las tramas
int actualizarDatos = 0;   //Indica el momento de actualizaci�n del RTC

//Definici�n de funciones
void retraso1TCYx(int val);
void retraso10KTCYx(int val);
int bcd(int dec);
int decimal(int bcd);
void inicializar();
void procesaTrama(int *h, int *m, int *s);
void corrigeHora(int *h, int *m, int *s);
void actualizaRTC(int hAct, int mAct, int sAct);
void descartaDato();
void almacenaReg(unsigned char valor);
void activaSegmentos(int val1, int val2);

//sentenciasISR es la rutina de interrupciones globales, es usada solo para
//procesar interrupciones debido a recepci�n en el m�dulo ASART, verifica que
//se recibe una trama, si la trama es GGA o RMC hace el llamado de procesaTrama
//y luego de actualizaRTC, de lo contrario llama a descartaDato
void interrupt sentenciasISR(void)
{
    char c;
    char tramaGPS[8];
    
    if(PIR1bits.RCIF == 0)
        return;

    c=ReadUSART();        
    if(c=='$'){
        getsUSART(tramaGPS,6);
        if(strstr(tramaGPS,TRAMA1) || strstr(tramaGPS,TRAMA2)){
            procesaTrama(&h0,&m0,&s0);
            actualizarDatos = 1;
        }
    }
    else
        descartaDato();
}

//Funci�n principal
void main(void)
{
    int hant=0;
    int mant=0;
    int h = 0;
    int m = 0;
    int s = 0;
    int cont = 0;
    
    int encendido = 0;
   
    inicializar();  //Funcion de inicializaci�n general
    while(1){
        //Al recibir alguna trama la procesa y actualiza el RTC
        
        //Enciende el led del pin RB7 cada 2 segundos aproximadamente
        if(cont < 1500 && !encendido){
            PORTB = PORTB | LED;
            encendido = 1;
        }
        if(1500 < cont && encendido){
            PORTB = PORTB & (~LED);
            encendido = 0;
        }
        if(3000<cont)
            cont=0;
        //Lectura de datos del RTC
        if(cont%1000==0){            
            h = decimal(leeDato(DIRHOR)&~(0X60));  //Almacena hora
            m = decimal(leeDato(DIRMIN));  //Almacena minutos
            s = decimal(leeDato(DIRSEG));  //Almacena segundos
        }
        //Habilitaci�n-Deshabilitaci�n de la interrupci�n durante el per�odo
        //de tiempo establecido
        if(s%30==0){
        //if(m!=mant){
            PIE1bits.RCIE=1;
            RCSTAbits.CREN=1;
            retraso10KTCYx(1); 
            PIE1bits.RCIE=0;
            RCSTAbits.CREN=0;
        }
        //Actualizaci�n del RTC
        if(actualizarDatos){
            actualizaRTC(h0,m0,s0);            
            actualizarDatos = 0;
        }
        //Encendiendo los 7 segmentos
        if(h!=hant || m!=mant)
            activaSegmentos(h, m);
        
        mant=m;
        hant=h;
        cont++;
    }
    //Deshabilita el m�dulo USART
    CloseUSART();
}

//Especificaci�n de funciones

//Permite crear pausas de "val" numero de ciclos de instrucci�n, 1TCY equivale
//a 4 ciclos del reloj 
void retraso1TCYx(int val)
{
    for(int i=0; i<val; i++)
        _delay(1);
}

//Crea pausas que son m�ltiplos de 10K ciclos de instrucci�n 
void retraso10KTCYx(int val)
{
    for(int i=0; i<val; i++)
        _delay(10000);
}

//bcd convierte un n�mero decimal al formato decimal codificado en binario (bcd)
int bcd(int dec)
{
    return((dec / 10) << 4) + (dec % 10);
}

//decimal convierte a decimal un dato en formato bcd
int decimal(int bcd)
{
    return ((bcd >> 4) * 10) + bcd % 16;
}

//inicializar contiene las instrucciones de configuraci�n e inicio para los
//puertos, perif�ricos y dispositivos
void inicializar()
{
    OSCCON = 0X72;  //Habilita el oscilador interno a 8 MHz
    //Configuraci�n de los puertos
    TRISA = 0XE0;
    TRISB = 0X6D;
    estadoB = 0X6D;  //Almacenamiento de la configuraci�n del puerto B
    //Inicializaci�n de los puertos
    PORTA = 0X00;
    PORTB = 0X12;
    //Inicializaci�n del RTC
    //inicializaRTC(bcd(SEGUNDOS), bcd(MINUTOS), bcd(HORA), bcd(FECHA),
    //              bcd(DIA), bcd(MES), bcd(ANIO), CTRL);
    ACTMR();  //Asigna un alto en MR y un bajo en OE
    //Comunicaci�n AUSART recepci�n continua, 
    //velocidad de transmisi�n a 9600 baudios
    OpenUSART();
    INTCONbits.GIE = 1;  //Habilitaci�n de interrupciones globales
    INTCONbits.PEIE = 1;  //Habilitaci�n de interrupciones de perifericos
    PIE1bits.RCIE = 1;  //Habilitaci�n de interrupci�n de recepci�n
                        //del periferico AUSART
    PIR1bits.RCIF = 0;  //Bit bandera de interrupci�n de recepci�n
                        //del periferico AUSART
    //Activaci�n de la tarjeta de d�gitos
    activaSegmentos(0, 0);
}

//procesaTrama es llamada al comenzar la interrupci�n por el periferico AUSART,
//extrae los caracteres entrantes en el puerto y separa los datos necesarios, 
//en este caso separa hora, minutos y segundos y los almacena como valores 
//decimales en los parametros pasados como referencia
void procesaTrama(int *h, int *m, int *s)
{  
    char hora[3];
    char minutos[3];
    char segundos[3];
    
    //Ejemplo de trama GGA:
    //$GPGGA,105540,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
    //Se extrae de la trama recibida la hora, minutos y segundos
    getsUSART(hora,2);
    getsUSART(minutos,2);
    getsUSART(segundos,2);
    segundos[2] = '\0';
    minutos[2] = '\0';
    hora[2] = '\0';
    
    while(!DataRdyUSART());
    descartaDato();
    //Convierte string a entero
    *h = atoi(hora);    
    *m = atoi(minutos);
    *s = atoi(segundos);
}

//corrigeHora recibe los datos de la hora del gps en formato UTC y los
//ajusta a la hora equivalente en Venezuela, esta funci�n es llamada
//en la funci�n actualizaRTC
void corrigeHora(int *h, int *m, int *s)
{
    if(*h<05)
        *h = (12 + *h) - 4;
    else if(04<*h && *h<17)
        *h = *h - 4;
    else if(16<*h)
        *h = (*h - 12) - 4;
    if(29<*m)
        *m = *m - 30;
    else if(*m<30){
        *m = *m + 30;
        *h = *h - 1;
        if(*h==0)
            *h = *h + 12;
    }
}

//actualizaRTC recibe la hora del gps, verifica si es horario am o pm, luego
//llama a la funci�n corrigeHora y luego env�a los datos por el puerto I2C
//para actualizar el RTC, usando la funci�n escribeDato de la librer�a ds1307.h
void actualizaRTC(int hAct, int mAct, int sAct)
{
    unsigned char ampm;
    
    ampm = 0X40;
    if(11<(hAct-4) || (hAct-4)<0)
        ampm = ampm | 0X20;

    corrigeHora(&hAct, &mAct, &sAct);

    escribeDato(0X02, bcd(hAct)|ampm);
    escribeDato(0X01, bcd(mAct));
    escribeDato(0X00, bcd(sAct));
}

//descartaDato se encarga de vaciar el registro de recepci�n del modulo USART
void descartaDato()
{    
    char dato[2];
    
    if(DataRdyUSART())
        while(1){
            dato[0] = ReadUSART();            
            if(dato[0]=='*'){
            //if(dato[0]==0X0D){
                getsUSART(dato,2);
                return;
            }
            while(!DataRdyUSART());
        }
}

//almacenaReg env�a al registro de desplazamiento las secuencias de bits
//para representar los valores le�dos del RTC en la tarjeta de d�gitos
void almacenaReg(unsigned char valor)
{    
    unsigned char aux;
    for(int i=0; i<8; i++){       
        aux = 0X00;
        aux = DATO;
        if((valor&0x01)==0X00)
            aux = aux & ~(DATO);
        valor = valor >> 1;
        PORTA = (aux | MR) & ~OE;
        _delay(5);
        PORTA = (aux | RELOJ | MR) & ~OE;
        _delay(5);
        PORTA = (aux | MR) & ~(RELOJ | OE);
    }   
}

//activaSegmentos hace el llamado de la funci�n almacenaReg, una vez que se
//han almacenado las secuencias de bits en los registros de desplazamiento
//correspondientes, se activa el bit LATCH, haciendo que estos bits pasen y se
//mantengan en los pines de salida paralela hasta recibir algun cambio
void activaSegmentos(int val1, int val2)
{
    char val[2];
    int u,d;

    sprintf(val,"%.2i",val1);
    
    u = val[1] -'0';
    d = val[0] -'0';
    
    almacenaReg(num[d]);
    almacenaReg(num[u]);

    sprintf(val,"%.2i",val2); 

    u = val[1] -'0';
    d = val[0] -'0';

    almacenaReg(num[d]);
    almacenaReg(num[u]);
    
    ACTLATCH();
    _delay(5);
    DACTLATCH();
}