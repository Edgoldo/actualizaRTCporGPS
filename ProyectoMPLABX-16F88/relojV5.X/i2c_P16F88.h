//Esta biblioteca de funciones está hecha en base a una biblioteca similar
//desarrollada por Microchip Technololy Inc. para el compilador XC8
//de nombre i2c.h, fue adaptada para usarse en este proyecto
//
//Realizada por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingeniería, 
//Escuela de Ingeniería de Sistemas
//Para el proyecto: Actualización de reloj de tiempo real mediante GPS
//De la empresa: Semáforos de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015

#ifndef __I2C_P16F88_H
#define	__I2C_P16F88_H

#include <htc.h>

//Pines de comunicación serial para el modo I2C del microprocesador
#define I2C_SCL	0X10
#define I2C_SDA	0X02

unsigned char estadoB;

void StartI2C( void );
void StopI2C( void );
void WriteI2C( unsigned char data_out );
unsigned char ReadI2C( void );

#endif	/* I2C_P16F88_H */

