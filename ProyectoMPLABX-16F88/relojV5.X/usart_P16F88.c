//Esta biblioteca de funciones est� hecha en base a un conjunto de bibliotecas
//que contienen funciones similares, fueron desarrolladas por Microchip 
//Technololy Inc. para el compilador XC8 y adaptadas para usarse en 
//este proyecto
//
//Realizada por el pasante: Edgar Antonio Linares Quintero
//Pasante de: Universidad de los Andes, Facultad de Ingenier�a, 
//Escuela de Ingenier�a de Sistemas
//Para el proyecto: Actualizaci�n de reloj de tiempo real mediante GPS
//De la empresa: Sem�foros de Venezuela C.A. SEMAVENCA
//Fecha: Agosto-Septiembre de 2015

#include <pic16f88.h>
#include "usart_P16F88.h"

//OpenUSART habilita el perif�rico AUSART del PIC16F88 para que trabaje
//a una velocidad de 9600 baudios, en recepci�n continua
void OpenUSART(void)
{
    TXSTA = 0X00;              //Reinicia los registros de transmisi�n y
    RCSTA = 0X00;              //recepci�n
    
    SPBRG = VAL_SPBRG;      //Escribe el valor para la tasa de baud
    RCSTAbits.CREN = 1;     //Recepci�n continua  
    TXSTAbits.BRGH = 1;     //Alta tasa de baud (velocidad))    
    //TXSTAbits.TXEN = 1;     //Habilitaci�n del transmisor
    RCSTAbits.SPEN = 1;     //Habilitaci�n del puerto serial
}

//DataRdyUSART verifica si hay recepci�n de caracteres por medio del bit 
//bandera de recepci�n del AUSART
char DataRdyUSART(void)
{
  if(PIR1bits.RCIF)
    return 1;  //Datos disponibles, devuelve verdadero
  return 0;  //Datos no disponibles, devuelve falso
}

//ReadUSART lee un byte de datos del perif�rico AUSART y lo devuelve
char ReadUSART(void)
{
  char data;
  
  data = RCREG; //Lee una palabra del registro RCREG
  return (data);
}

//WriteUSART, permite enviar un byte de datos por el transmisor del AUSART
void WriteUSART(char data)
{
  TXREG = data;  //Escribe la palabra en el registro TXREG
}

//getsUSART lee una cadena de "len" n�mero de caracteres y los almacena en el 
//par�metro buffer, 
void getsUSART(char *buffer, unsigned char len)
{
  char i;  //Contador
  unsigned char data;  //Auxiliar para guardar cada palabra
  int cont = 0;
  char aux[2];

  for(i=0;i<len;i++)  //Repite len veces
  {
    while(!DataRdyUSART());   //Verifica que hay datos disponibles

    data = ReadUSART();  //Obtiene una palabra a la vez y
    *buffer = data;  //la almacena en la posici�n correspondiente
    buffer++;  //Incrementa el puntero de la cadena
  }
}

//putsUSART transmite una cadena de caracteres por el perif�rico AUSART
void putsUSART( char *data)
{
  do
  {
    while(BusyUSART());  //Asegura la disponibilidad del perif�rico 
    WriteUSART(*data);  //Escribe un byte a la vez
  } while( *data++ );  //Aumenta el puntero de la cadena si es posible
}

//BusyUSART verifica que el transmisor del perif�rico AUSART est� vacio
char BusyUSART(void)
{
  if(!TXSTAbits.TRMT)  
    return 1;          //No est� vacio, devuelve falso
  return 0;            //Est� vacio, devuelve verdadero
}

//CloseUSART deshabilita el receptor y el transmisor y finaliza la comunicaci�n
//por el perif�rico AUSART
void CloseUSART(void)
{
  RCSTA &= 0b01001111;
  TXSTAbits.TXEN = 0;
  PIE1 &= 0b11001111;
}